# Isobar Challenge
Author: Michael Hargreaves

## Quickstart
1. Clone the repo. 
```
    git clone git@bitbucket.org:mickatron/isobar-challenge.git
```
2. Install dependencies.    
```
    npm install
    # or
    yarn install
```
3. From the project root run;
```    
    npm start
    # or
    yarn start
```
If the demo does not automatically open in your default browser visit the local address given in your terminal.

## Running Tests
To run test and produce a coverage report run
```    
    npm run cover
    # or
    yarn cover
```
You can run all tests and watch for changes with
```    
    yarn test
```
### Other NPM scripts
Refer to package.json
 

 
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). It was just a quick easy and fairly solid starting point, I have not ejected.
