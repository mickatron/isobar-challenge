import { shallow } from 'enzyme';
import React from 'react';

import PuzzleTile from './PuzzleTile';

describe('PuzzleTile', () => {

  describe('renders', () => {
    let component;
    const mockClickHanlder = jest.fn();

    beforeAll(()=> {
      component = shallow(
        <PuzzleTile
          className="test"
          onClick={mockClickHanlder}
        >
          1
        </ PuzzleTile>
      );
    });

    it('with custom classname', () => {
      expect(component).toMatchSnapshot();
      expect(component.find('.PuzzleTile.test').exists()).toBeTruthy();
    });

    it('and can be clicked', () => {
      component.find('.PuzzleTile.test').simulate('click');
      expect(mockClickHanlder).toHaveBeenCalled();
    });

  });

});