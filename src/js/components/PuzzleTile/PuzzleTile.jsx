import './PuzzleTile.css';

import classnames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import classNameType from 'js/proptypes/className';

class PuzzleTile extends React.PureComponent {
  
  static propTypes = {
    children: PropTypes.node,
    className: classNameType,
    data: PropTypes.any,
    onClick: PropTypes.func,
    style: PropTypes.object,
  }
  
  handlers = {
    onClick: (event) => {
      const { data, onClick } = this.props;
      if (onClick) onClick(event, data);
    },
  }

  render() {
    const {
      children,
      className,
      onClick,
      style,
    } = this.props;
    return (
      <div 
        className={classnames(className, 'PuzzleTile')}
        onClick={onClick ? this.handlers.onClick : null} 
        style={style}
      >
        {children}
      </div>
    );
  }
}

export default PuzzleTile;
