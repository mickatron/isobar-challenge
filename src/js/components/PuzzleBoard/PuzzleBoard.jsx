import './PuzzleBoard.css';

import classnames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import classNameType from 'js/proptypes/className';

class PuzzleBoard extends React.PureComponent {
  
  static propTypes = {
    children: PropTypes.node,
    className: classNameType,
  }

  render() {
    const {
      children,
      className,
    } = this.props;
    return (
      <div className={classnames(className, 'PuzzleBoard')} >
        {children}
      </div>
    );
  }
}

export default PuzzleBoard;
