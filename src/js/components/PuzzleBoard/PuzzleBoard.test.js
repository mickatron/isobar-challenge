import { shallow } from 'enzyme';
import React from 'react';

import PuzzleBoard from './PuzzleBoard';

describe('PuzzleBoard', () => {

  describe('renders', () => {

    let component;
    beforeAll(() => {
      component = shallow(
        <PuzzleBoard
          className="test"
        >
          some child text
        </ PuzzleBoard>
      );
    });

    it('with custom classname', () => {
      expect(component).toMatchSnapshot();
      expect(component.find('.PuzzleBoard.test').exists()).toBeTruthy();
    });

  });
});