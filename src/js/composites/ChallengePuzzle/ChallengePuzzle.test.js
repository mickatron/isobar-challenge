
import { mount } from 'enzyme';
import React from 'react';

import ChallengePuzzle from './ChallengePuzzle';

describe('ChallengePuzzle', () => {

  let component;
  let originalOrder;
  beforeAll(() => {
    component = mount(
      <ChallengePuzzle
        className="test"
        divisions={3}
      />

    );
  });
  
  describe('renders', () => {

    it('with custom classname', () => {
      expect(component.find('.ChallengePuzzle.test').exists()).toBeTruthy();
    });

    it('has correct ammount of tiles', () => {
      expect(component.find('.ChallengePuzzle__tile')).toHaveLength(9);
    });

    it('has correct original order', () => {
      originalOrder = component.instance().createOriginalOrder(1);
      expect(originalOrder).toMatchSnapshot();
    });
    
  });
  
  it('gets the coordinates of the tile from its array index', () => {
    expect(component.instance().getCoords(1)).toEqual({ x: 1, y: 1 });
  });

  it('correctly validates the tile can be moved', () => {
    expect(component.instance().isMoveValid(0, 2)).toEqual(false);

    expect(component.instance().isMoveValid(0, 3)).toEqual(true);

    expect(component.instance().isMoveValid(0, 7)).toEqual(false);
  });
  
  /*
    // Ran out of time  
    describe('clicked tile', () => {
      it('swaps position', () => {
      });
    });
  */

  describe('solved', () => {

    beforeAll(() => { 
      component.setState({
        currentOrder: originalOrder,
        isComplete: true,
      });
    });

    it('displays success message', () => {

      expect(component.find('.ChallengePuzzle__success').exists()).toBeTruthy();
    });
  });
  
});