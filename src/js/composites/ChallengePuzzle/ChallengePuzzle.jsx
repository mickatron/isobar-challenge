import './ChallengePuzzle.css';

import classnames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import isEqual from 'lodash/isEqual';
import shuffle from 'lodash/shuffle';

import PuzzleBoard from 'js/components/PuzzleBoard/PuzzleBoard';
import PuzzleTile from 'js/components/PuzzleTile/PuzzleTile';

class ChallengePuzzle extends React.PureComponent {

  static propTypes = {
    divisions: PropTypes.number, //  The number of column and row divisions
  }

  static defaultProps = {
    divisions: 4,
  }

  originalOrder = this.createOriginalOrder();

  state = {
    currentOrder: this.shuffleOrder(),
    isComplete: false,
  };

  createOriginalOrder() {
    const order = ['empty'];
    const { divisions } = this.props;
    // construct puzzle data
    const numberOfPieces = (divisions * divisions) - 1;
    for (let i = numberOfPieces; i > 0; i--) {
      order.unshift(i);
    }
    return order;
  }

  shuffleOrder() {
    const newOrder = shuffle(this.originalOrder);
    // shuffle does not guarantee a different order
    // recurse if the order is complete to ensure a shuffled order.
    while(this.isPuzzleComplete(newOrder)) {
      return this.shuffleOrder(newOrder);
    }
    return newOrder;
  }

  isPuzzleComplete(newOrder) { 
    return isEqual(newOrder, this.originalOrder);
  }

  getCoords(arrayIndex) {
    const { divisions } = this.props;
    // calculate the x/y cooordinates of the tile on the board 
    const y = Math.floor(arrayIndex / divisions)+1;
    return { 
      x: arrayIndex - (divisions * (y - 1)), 
      y,
    };
  }

  isMoveValid(emptyArrayIndex, targetArrayIndex) {
    const emptyTileCoords = this.getCoords(emptyArrayIndex);
    const targetTileCoords = this.getCoords(targetArrayIndex);

    const coordDifference = {
      x: emptyTileCoords.x - targetTileCoords.x,
      y: emptyTileCoords.y - targetTileCoords.y,
    };
    
    // the target tile needs to be adjacent to the empty tile
    // if the position difference is between -1 & 1 that will be true
    return (
      coordDifference.x >= -1 && coordDifference.y >= -1
      &&
      coordDifference.x <= 1 && coordDifference.y <= 1
    );
  }

  handlers = {
    tileClick: (event, tileValue) => {
      if (tileValue ==='empty') return;
      const { currentOrder } = this.state;
      const emptyArrayIndex = currentOrder.indexOf('empty');
      const tileArrayIndex = currentOrder.indexOf(tileValue);
      
      // can the piece be moved to the empty square?
      if (this.isMoveValid(emptyArrayIndex, tileArrayIndex)) {
        // valid move: edit tile position
        const newOrder = [ ...currentOrder ];
        newOrder[emptyArrayIndex] = tileValue;
        newOrder[tileArrayIndex] = 'empty';
        // update state with new tile order and test if the puzzle is complete
        this.setState({ 
          currentOrder: newOrder, 
          isComplete: this.isPuzzleComplete(newOrder),
        });
      }
      // invalid moves do nothing
    },
  };

  /* 
    RENDERING METHODS
  \* --------------------------------------- */
  render() {
    const { className } = this.props;
    const { isComplete } = this.state;

    return (
      <div className={classnames(className, 'ChallengePuzzle')}>
        <h1>Isobar challenge puzzle</h1>
        <p>To complete the challenge the numbers must be in order with the empty 
          tile in the bottom right corner.</p>
        { isComplete ?
          (
            <div className="ChallengePuzzle__success">
              Puzzle done!
            </div>
          ) :
          (
            <PuzzleBoard
              className="ChallengePuzzle__board"
            >
              { this.renderTiles() }
            </PuzzleBoard>
          )
        }
      </div>
    );
  }

  renderTiles() {
    const { divisions } = this.props;
    const { currentOrder } = this.state;
    
    const styleObject = {
      width: `calc(${100 / divisions}%)`,
    };

    return currentOrder.map((tileValue, i)=> {
      return (
        <PuzzleTile
          className={['ChallengePuzzle__tile', `ChallengePuzzle__tile--${tileValue}`]}
          data={tileValue}
          key={tileValue}
          onClick={this.handlers.tileClick}
          style={styleObject}
        >
          {tileValue}
        </PuzzleTile>
      );
    });
  }

}

export default ChallengePuzzle;