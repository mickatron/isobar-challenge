import './App.css';

import React from 'react';

import ChallengePuzzle from 'js/composites/ChallengePuzzle/ChallengePuzzle';

class App extends React.PureComponent {
 
  render() {
    return (
      <div className="App">
        <ChallengePuzzle 
          divisions={3}
        />
      </div>
    );
  }
}

export default App;
